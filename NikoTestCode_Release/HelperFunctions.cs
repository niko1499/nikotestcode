﻿using System;
using RSI.RapidCode.dotNET;         // Import our RapidCode Library.
using RSI.RapidCode.dotNET.Enums;
namespace SampleAppsCS
{
    public static class HelperFunctions
    {
        public static void CheckErrors(IRapidCodeObject rsiObject)
        {
            while (rsiObject.ErrorLogCountGet() > 0)
            {
                Console.WriteLine("RSI Object: " + rsiObject + ")\n\n " + rsiObject.ErrorLogGet().Message);
            }
        }
        public static void StartTheNetwork(MotionController controller)
        {
            // Initialize the Network
            if (controller.NetworkStateGet() != RSINetworkState.RSINetworkStateOPERATIONAL)         // Check if network is started already.
            {
                Console.WriteLine("Starting Network..");
                controller.NetworkStart();                                                          // If not. Initialize The Network. (This can also be done from RapidSetup Tool)
            }
            if (controller.NetworkStateGet() != RSINetworkState.RSINetworkStateOPERATIONAL)         // Check if network is started again.
            {
                int messagesToRead = controller.NetworkLogMessageCountGet();                        // Some kind of error starting the network, read the network log messages
                for (int i = 0; i < messagesToRead; i++)
                {
                    Console.WriteLine(controller.NetworkLogMessageGet(i));                          // Print all the messages to help figure out the problem
                }
                Console.WriteLine("Expected OPERATIONAL state but the network did not get there.");
                //throw new RsiError();                                                             // Uncomment if you want your application to exit when the network isn't operational. (Comment when using phantom axis)
            }
            else                                                                                    // Else, of network is operational.
            {
                Console.WriteLine("Network Started");
            }
        }
    }
}