﻿using RSI.RapidCode.dotNET;         // Import our RapidCode Library.
using RSI.RapidCode.dotNET.Enums;
using System;

namespace SampleAppsCS
{
    class DS402HomeTest
    {
        static void Main(string[] args)
        {

            // constants
            const int AXIS_NUMBER = 0;          // Specify which axis/motor to control.
            const int USER_UNITS = 1048576;     // Specify your counts per unit / user units.           (the motor used in this sample app has 1048576 encoder pulses per revolution) 
            const double VELOCITY = 1;             // Specify your velocity.       -   units: Units/Sec    (it will do 1048576 counts/1 revolution every 1 second.)
            const double ACCELERATION = 10;        // Specify your acceleration.   -   units: Units/Sec^2
            const double DECELERATION = 10;        // Specify your deceleration.   -   units: Units/Sec^2

            // Initialize RapidCode Objects
            MotionController controller = MotionController.CreateFromSoftware(/*@"C:\RSI\X.X.X\"*/);    // Insert the path location of the RMP.rta (usually the RapidSetup folder) 
            SampleAppsCS.HelperFunctions.CheckErrors(controller);                                   // [Helper Function] Check that the controller has been initialize correctly.
            SampleAppsCS.HelperFunctions.StartTheNetwork(controller);                               // [Helper Function] Initialize the network.            
            //controller.AxisCountSet(1);                                                           // Uncomment if using Phantom Axes.
            Axis axis = controller.AxisGet(AXIS_NUMBER);                                            // Initialize Axis Class. (Use RapidSetup Tool to see what is your axis number)
            SampleAppsCS.HelperFunctions.CheckErrors(axis);                                         // [Helper Function] Check that the axis has been initialize correctly.

            try
            {
                axis.UserUnitsSet(USER_UNITS);                                                      // Specify the counts per Unit.

                axis.Abort();                                                                       // If there is any motion happening, abort it.
                axis.ClearFaults();                                                                 // Clear faults.
                axis.AmpEnableSet(true);                                                            // Enable the motor.
                //axis.HardwareNegLimitActionSet(RSIAction.RSIActionSTOP);                            // Neg Limit action set to STOP.
                //axis.HardwarePosLimitActionSet(RSIAction.RSIActionSTOP);                            // Neg Limit action set to STOP.
                axis.ErrorLimitTriggerValueSet(1);

                axis.HomeMethodSet((RSIHomeMethod)124);                      // Set the method to be used for homing.                  
                axis.HomeVelocitySet(VELOCITY);                                                     // Set the home velocity.
                axis.HomeVelocitySet(RSIHomeStage.RSIHomeStageSTAGE_TWO, VELOCITY / 10);
                axis.HomeVelocitySet(RSIHomeStage.RSIHomeStageSTAGE_THREE, VELOCITY / 20);
                axis.HomeAccelerationSet(ACCELERATION);                                             // Set the acceleration used for homing.
                axis.HomeDecelerationSet(DECELERATION);                                             // Set the deceleration used for homing.
                axis.HomeOffsetSet(0.0);                                                            // HomeOffsetSet sets the position offset from the home (zero) position.

                axis.Home();                                                                        // Execute the homing routine.

                if (axis.HomeStateGet() == true)                                                    // HomeStateGet returns true if the Axis is homed.
                {
                    Console.WriteLine("Homing successful\n");
                }

                axis.ClearFaults();                                                                 // Clear faults created by homing.
                axis.AmpEnableSet(false);                                                           // Disable the motor.
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            Console.WriteLine("\nPress Any Key To Exit");                                         // Allow time to read Console.
            Console.ReadKey();
        }
    }
}